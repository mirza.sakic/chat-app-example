#include <json/json.hpp>
#include <uWebSockets/App.hpp>
#include <unordered_map>

using namespace std::chrono_literals;

namespace JsonMsgs {

struct Connect {
    std::string msgType;
    std::string username;
};

void to_json(nlohmann::json& j, const Connect& c) {
    j = nlohmann::json{{"msgType", c.msgType}, {"username", c.username}};
}

void from_json(const nlohmann::json& j, Connect& c) {
    j.at("msgType").get_to(c.msgType);
    j.at("username").get_to(c.username);
}

struct NewMessage {
    std::string msgType;
    std::string content;
};

void to_json(nlohmann::json& j, const NewMessage& c) {
    j = nlohmann::json{{"msgType", c.msgType}, {"text", c.content}};
}

void from_json(const nlohmann::json& j, NewMessage& c) {
    j.at("msgType").get_to(c.msgType);
    j.at("content").get_to(c.content);
}

struct MessageBroadcast {
    std::string msgType;
    std::string username;
    std::string text;
};

void to_json(nlohmann::json& j, const MessageBroadcast& c) {
    j = nlohmann::json{
        {"msgType", c.msgType}, {"username", c.username}, {"text", c.text}};
}

void from_json(const nlohmann::json& j, MessageBroadcast& c) {
    j.at("msgType").get_to(c.msgType);
    j.at("text").get_to(c.text);
    j.at("username").get_to(c.username);
}

}  // namespace JsonMsgs

struct PodaciOKonekciji {
    std::string username;
};

int main() {
    uWS::App app;
    uWS::App::WebSocketBehavior<PodaciOKonekciji> config;
    using WebSocket = uWS::WebSocket<false, true, PodaciOKonekciji>;
    std::vector<WebSocket*> activeConnections;
    std::vector<JsonMsgs::MessageBroadcast> history;
    config.open = [](auto* ws) {
        std::cout << "Konekcija otvorena" << std::endl;
    };
    config.close = [&activeConnections](auto* ws, int code,
                                        std::string_view s) {
        std::cout << "Konekcija zatvorena!" << std::endl;
        auto it =
            std::find(activeConnections.begin(), activeConnections.end(), ws);
        if (it != std::end(activeConnections)) {
            activeConnections.erase(it);
        }
    };
    config.message = [&activeConnections, &history](auto* ws, std::string_view msg,
                                          uWS::OpCode op) {
        auto j = nlohmann::json::parse(msg);
        std::cout << "MsgType: " << j["msgType"] << std::endl;
        if (j["msgType"] == "connect") {
            JsonMsgs::Connect connection = j.get<JsonMsgs::Connect>();
            std::cout << "User " << connection.username << " connected"
                      << std::endl;
            ws->getUserData()->username = connection.username;
            activeConnections.emplace_back(ws);
            for (auto&& msg : history) {
                ws->send(nlohmann::json(msg).dump(), op, false);
            }
            std::this_thread::sleep_for(10s);
        } else if (j["msgType"] == "newMessage") {
            JsonMsgs::NewMessage newMsg = j.get<JsonMsgs::NewMessage>();
            std::cout << "New message received: " << newMsg.content
                      << std::endl;
            JsonMsgs::MessageBroadcast msgBroadcast;
            msgBroadcast.text = newMsg.content;
            msgBroadcast.username = ws->getUserData()->username;
            msgBroadcast.msgType = "MessageBroadcast";
            history.push_back(msgBroadcast);
            auto jsonMsg = nlohmann::json(msgBroadcast);
            for (auto&& conn : activeConnections)
                conn->send(jsonMsg.dump(), op, false);
        }
    };
    app.ws("/*", std::move(config));
    app.listen(10000, [](auto* listenSocket) {
        if (listenSocket) {
            std::cout << "Websocket server running on: " << 10000 << std::endl;
        } else {
            std::cout << "Couldn't listen on port 10000!" << std::endl;
        }
    });
    app.run();
    return 0;
}

